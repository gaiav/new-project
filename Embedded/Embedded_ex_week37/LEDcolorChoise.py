#pin 17 for the red LED
#pin 27 for the yellow LED
#pin 22 for the green LED

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)


while True:
    
  print("Choose color of LED (RED, YELLOW, GREEN) :")

  LEDcolor = input()

  if LEDcolor == "RED" :
      GPIO.output(17, True)
      time.sleep(2)
      GPIO.setup(17, False)
  elif LEDcolor == "YELLOW" :
      GPIO.output(27, True)
      time.sleep(2)
      GPIO.setup(27, False)
  elif LEDcolor == "GREEN" :
      GPIO.output(22, True)
      time.sleep(2)
      GPIO.setup(22, False)
 


